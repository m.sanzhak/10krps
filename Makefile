PROJECT_NAME=10krps
CURRENT_DIR?=$(shell pwd)
SERVICE_PORT=8010

up:
	docker-compose build --no-cache && docker-compose up

lint:
	@docker run --name $(PROJECT_NAME)-lint --rm -i -v "$(CURRENT_DIR):/$(PROJECT_NAME)" -e "GOPATH=/$(PROJECT_NAME)" -w /$(PROJECT_NAME) golangci/golangci-lint:v1.27-alpine golangci-lint run ./src/$(PROJECT_NAME)/... -E gofmt --skip-dirs=./src/$(PROJECT_NAME)/vendor --timeout=10m
