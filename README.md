# Запуск

```make up``` - поднимается сразу сервис с Redis'ом, внутри к-го уже будут 
все данные. Создать сеть: 
```shell
docker network create 10krps
```

# Тестирование
Использовал Apache AB
```shell
docker run --rm jordi/ab -k -c 10000 -n 10000 http://172.17.0.1:8010/json/hackers
```
Вывод у меня на mbp 2020:
```shell
This is ApacheBench, Version 2.3 <$Revision: 1826891 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 172.17.0.1 (be patient)
Completed 1000 requests
Completed 2000 requests
Completed 3000 requests
Completed 4000 requests
Completed 5000 requests
Completed 6000 requests
Completed 7000 requests
Completed 8000 requests
Completed 9000 requests
Completed 10000 requests
Finished 10000 requests


Server Software:        
Server Hostname:        172.17.0.1
Server Port:            8010

Document Path:          /json/hackers
Document Length:        232 bytes

Concurrency Level:      10000
Time taken for tests:   4.535 seconds
Complete requests:      10000
Failed requests:        0
Keep-Alive requests:    10000
Total transferred:      3650000 bytes
HTML transferred:       2320000 bytes
Requests per second:    2205.08 [#/sec] (mean)
Time per request:       4534.982 [ms] (mean)
Time per request:       0.453 [ms] (mean, across all concurrent requests)
Transfer rate:          785.99 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0  519 333.9    414    1130
Processing:   429 1870 676.1   1840    3432
Waiting:        2 1870 676.2   1840    3432
Total:        474 2388 816.9   1961    3957

Percentage of the requests served within a certain time (ms)
  50%   1961
  66%   3026
  75%   3115
  80%   3181
  90%   3660
  95%   3711
  98%   3734
  99%   3783
 100%   3957 (longest request)
```