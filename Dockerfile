FROM golang:1.14.3

ADD . /10krps

WORKDIR /10krps
ENV GOPATH=/10krps

RUN go build -o /tmp/10krps ./src/10krps/main.go

RUN chmod +x /tmp/10krps

EXPOSE 8010
CMD ["/tmp/10krps"]