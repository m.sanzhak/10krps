module 10krps

go 1.15

require (
	github.com/caarlos0/env/v6 v6.6.2
	github.com/go-redis/redis/v8 v8.10.0
	github.com/gofiber/fiber/v2 v2.11.0
	github.com/sirupsen/logrus v1.8.1
)
