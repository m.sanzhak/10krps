package handlers

import (
	"github.com/go-redis/redis/v8"
)

func toHackers(z []redis.Z) *Hackers {
	h := Hackers{}
	for i := range z {
		h = append(h, Hacker{
			Name:  z[i].Member.(string),
			Score: int(z[i].Score),
		})
	}
	return &h
}
