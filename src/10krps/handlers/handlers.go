package handlers

import (
	"net/http"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
)

type cache interface {
	Get(key string) ([]redis.Z, error)
}

type Handler struct {
	cache cache
}

func New(cache cache) *Handler {
	return &Handler{cache}
}

func (h *Handler) GetHackers(c *fiber.Ctx) error {
	res, err := h.cache.Get("hackers")
	if err != nil {
		log.WithError(err).Error("get cache err")
		_ = c.JSON(map[string]interface{}{
			"error":  err.Error(),
			"status": http.StatusInternalServerError,
		})
	}

	return c.JSON(toHackers(res))
}
