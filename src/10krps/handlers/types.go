package handlers

type Hackers []Hacker

type Hacker struct {
	Name  string `json:"name"`
	Score int    `json:"score"`
}
