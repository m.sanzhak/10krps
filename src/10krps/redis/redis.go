package redis

import (
	"context"

	"github.com/go-redis/redis/v8"
)

type Redis struct {
	client *redis.Client
}

func New(a, p string, db int) (*Redis, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     a,
		Password: p,
		DB:       db,
	})

	err := client.Ping(context.TODO()).Err()
	if err != nil {
		return nil, err
	}
	return &Redis{client}, nil
}

func (r *Redis) Get(key string) ([]redis.Z, error) {
	res, err := r.client.ZRangeWithScores(context.TODO(), key, 0, -1).Result()
	if err != nil {
		return nil, err
	}

	return res, nil
}
