package main

import (
	"10krps/handlers"
	"10krps/redis"

	"github.com/caarlos0/env/v6"
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
)

type Config struct {
	ServerPort string `env:"SERVER_PORT" envDefault:"8010"`
	LogLevel   string `env:"LOG_LEVEL" envDefault:"info"`
	RedisURL   string `env:"REDIS_URL,required"`
	RedisPass  string `env:"REDIS_PASS" envDefault:""`
	RedisDB    int    `env:"REDIS_DB" envDefault:"0"`
}

func readConfig() (Config, error) {
	cfg := Config{}
	err := env.Parse(&cfg)
	if err != nil {
		return Config{}, err
	}

	log.SetReportCaller(true)
	log.SetFormatter(&log.JSONFormatter{})
	if level, err := log.ParseLevel(cfg.LogLevel); err == nil {
		log.SetLevel(level)
	} else {
		log.WithField("level", cfg.LogLevel).Warn("Invalid log level")
	}

	return cfg, err
}

func main() {
	server := fiber.New()
	config, err := readConfig()
	if err != nil {
		log.WithError(err).Error("read config err")
		return
	}

	cache, err := redis.New(config.RedisURL, config.RedisPass, config.RedisDB)
	if err != nil {
		log.WithError(err).Error("create redis client err")
		return
	}

	hackersHandlers := handlers.New(cache)

	jsonGroup := server.Group("/json")
	jsonGroup.Get("/hackers", hackersHandlers.GetHackers)

	log.Fatal(server.Listen(":" + config.ServerPort))
}
